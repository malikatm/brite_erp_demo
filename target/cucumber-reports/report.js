$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/analysisChart.feature");
formatter.feature({
  "name": "Expenses report",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@analysis"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Manager should be able see Expenses Analysis Chart",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@analysis"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user successfully login",
  "keyword": "Given "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_successfully_login()"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: [[ChromeDriver: chrome on MAC (d33df7b13e26f7ed1c8ee946929cde44)] -\u003e xpath: //a[@class\u003d\u0027oe_menu_toggler\u0027]/span[contains(text(),\u0027Expenses\u0027)]] (tried for 10 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027Malikas-MacBook.local\u0027, ip: \u00272603:300a:16aa:1000:0:0:0:7696%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14\u0027, java.version: \u00271.8.0_172\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 73.0.3683.20 (8e2b610813e16..., userDataDir: /var/folders/pf/vcygvx_97cq...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:51131}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), rotatable: false, setWindowRect: true, strictFileInteractability: false, takesHeapSnapshot: true, takesScreenshot: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unexpectedAlertBehaviour: ignore, unhandledPromptBehavior: ignore, version: 71.0.3578.98, webStorageEnabled: true}\nSession ID: d33df7b13e26f7ed1c8ee946929cde44\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:113)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:283)\n\tat utilities.BrowserUtilities.waitForClickablility(BrowserUtilities.java:29)\n\tat step_definitions.ExpenseCreation_steps.user_successfully_login(ExpenseCreation_steps.java:30)\n\tat ✽.user successfully login(src/test/resources/features/analysisChart.feature:6)\n",
  "status": "failed"
});
formatter.step({
  "name": "manager sees Expenses Analysis Chart in pieChart view",
  "keyword": "Then "
});
formatter.match({
  "location": "AnalysisChart.manager_sees_Expenses_Analysis_Chart_in_pieChart_view()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "manager sees Expenses Analysis Chart in BarChart view",
  "keyword": "Then "
});
formatter.match({
  "location": "AnalysisChart.manager_sees_Expenses_Analysis_Chart_in_BarChart_view()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "manager sees Expenses Analysis Chart in LineChart view",
  "keyword": "Then "
});
formatter.match({
  "location": "AnalysisChart.manager_sees_Expenses_Analysis_Chart_in_LineChart_view()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/features/expenseCreation.feature");
formatter.feature({
  "name": "Testing my expense creation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@creation"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Verify that expenses created successfully",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@creation"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user successfully login",
  "keyword": "Given "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_successfully_login()"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: [[ChromeDriver: chrome on MAC (06f204c8e9de28027e165df2635c00ef)] -\u003e xpath: //a[@class\u003d\u0027oe_menu_toggler\u0027]/span[contains(text(),\u0027Expenses\u0027)]] (tried for 10 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027Malikas-MacBook.local\u0027, ip: \u00272603:300a:16aa:1000:0:0:0:7696%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14\u0027, java.version: \u00271.8.0_172\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 73.0.3683.20 (8e2b610813e16..., userDataDir: /var/folders/pf/vcygvx_97cq...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:51160}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), rotatable: false, setWindowRect: true, strictFileInteractability: false, takesHeapSnapshot: true, takesScreenshot: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unexpectedAlertBehaviour: ignore, unhandledPromptBehavior: ignore, version: 71.0.3578.98, webStorageEnabled: true}\nSession ID: 06f204c8e9de28027e165df2635c00ef\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:113)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:283)\n\tat utilities.BrowserUtilities.waitForClickablility(BrowserUtilities.java:29)\n\tat step_definitions.ExpenseCreation_steps.user_successfully_login(ExpenseCreation_steps.java:30)\n\tat ✽.user successfully login(src/test/resources/features/expenseCreation.feature:7)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on create button",
  "keyword": "When "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_clicks_on_create_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user fill out expenses form",
  "keyword": "And "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_fill_out_expenses_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see confirmation that expense is created",
  "keyword": "Then "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_should_see_confirmation_that_expense_is_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/features/expenseCreationWithSubmission.feature");
formatter.feature({
  "name": "Testing my expense creation with submission",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@submission"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Verify submit button functionality",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@submission"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user successfully login",
  "keyword": "Given "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_successfully_login()"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: [[ChromeDriver: chrome on MAC (0ebc22f9abb1d84f102111858724fc5f)] -\u003e xpath: //a[@class\u003d\u0027oe_menu_toggler\u0027]/span[contains(text(),\u0027Expenses\u0027)]] (tried for 10 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027Malikas-MacBook.local\u0027, ip: \u00272603:300a:16aa:1000:0:0:0:7696%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14\u0027, java.version: \u00271.8.0_172\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 73.0.3683.20 (8e2b610813e16..., userDataDir: /var/folders/pf/vcygvx_97cq...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:51187}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), rotatable: false, setWindowRect: true, strictFileInteractability: false, takesHeapSnapshot: true, takesScreenshot: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unexpectedAlertBehaviour: ignore, unhandledPromptBehavior: ignore, version: 71.0.3578.98, webStorageEnabled: true}\nSession ID: 0ebc22f9abb1d84f102111858724fc5f\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:113)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:283)\n\tat utilities.BrowserUtilities.waitForClickablility(BrowserUtilities.java:29)\n\tat step_definitions.ExpenseCreation_steps.user_successfully_login(ExpenseCreation_steps.java:30)\n\tat ✽.user successfully login(src/test/resources/features/expenseCreationWithSubmission.feature:7)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on create button",
  "keyword": "When "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_clicks_on_create_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should be able to enter \"Out of state trip\" to expense description box.",
  "keyword": "And "
});
formatter.match({
  "location": "ExpenseCreationWithSubmission.user_should_be_able_to_enter_to_expense_description_box(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects \"[CarTRA] Car Travel Expenses\" from drop down.",
  "keyword": "When "
});
formatter.match({
  "location": "ExpenseCreationWithSubmission.user_selects_from_drop_down(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should be able to select required employee.",
  "keyword": "And "
});
formatter.match({
  "location": "ExpenseCreationWithSubmission.user_should_be_able_to_select_required_employee()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enters additional notes in notes box",
  "keyword": "And "
});
formatter.match({
  "location": "ExpenseCreationWithSubmission.user_enters_additional_notes_in_notes_box()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "click on {string} button",
  "keyword": "Then "
});
formatter.match({
  "location": "ExpenseCreationWithSubmission.click_on_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded2.png");
formatter.after({
  "status": "passed"
});
formatter.uri("src/test/resources/features/negative_expenseCreation.feature");
formatter.feature({
  "name": "Testing expenses module website",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@negative"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.scenario({
  "name": "Testing creating new expense report with blank form",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@negative"
    },
    {
      "name": "@smoke"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user successfully login",
  "keyword": "Given "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_successfully_login()"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: [[ChromeDriver: chrome on MAC (ed482333bd38050a2ebe5b1a0a2509dc)] -\u003e xpath: //a[@class\u003d\u0027oe_menu_toggler\u0027]/span[contains(text(),\u0027Expenses\u0027)]] (tried for 10 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027Malikas-MacBook.local\u0027, ip: \u00272603:300a:16aa:1000:0:0:0:7696%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14\u0027, java.version: \u00271.8.0_172\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 73.0.3683.20 (8e2b610813e16..., userDataDir: /var/folders/pf/vcygvx_97cq...}, cssSelectorsEnabled: true, databaseEnabled: false, goog:chromeOptions: {debuggerAddress: localhost:51216}, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), rotatable: false, setWindowRect: true, strictFileInteractability: false, takesHeapSnapshot: true, takesScreenshot: true, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unexpectedAlertBehaviour: ignore, unhandledPromptBehavior: ignore, version: 71.0.3578.98, webStorageEnabled: true}\nSession ID: ed482333bd38050a2ebe5b1a0a2509dc\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:113)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:283)\n\tat utilities.BrowserUtilities.waitForClickablility(BrowserUtilities.java:29)\n\tat step_definitions.ExpenseCreation_steps.user_successfully_login(ExpenseCreation_steps.java:30)\n\tat ✽.user successfully login(src/test/resources/features/negative_expenseCreation.feature:7)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on create button",
  "keyword": "When "
});
formatter.match({
  "location": "ExpenseCreation_steps.user_clicks_on_create_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user leave expense report blank",
  "keyword": "And "
});
formatter.match({
  "location": "NegativeExpenseCreation_steps.user_leave_expense_report_blank()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user clicks on save button",
  "keyword": "And "
});
formatter.match({
  "location": "NegativeExpenseCreation_steps.user_clicks_on_save_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see a pop up notification",
  "keyword": "Then "
});
formatter.match({
  "location": "NegativeExpenseCreation_steps.user_should_see_a_pop_up_notification()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded3.png");
formatter.after({
  "status": "passed"
});
});