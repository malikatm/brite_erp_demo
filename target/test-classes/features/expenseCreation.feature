@creation
@smoke
Feature: Testing my expense creation

  Scenario: Verify that expenses created successfully

    Given user successfully login

    When user clicks on create button
    And user fill out expenses form
    Then user should see confirmation that expense is created

