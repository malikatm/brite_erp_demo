
@analysis
@smoke
Feature: Expenses report
  Scenario: Manager should be able see Expenses Analysis Chart
    Given user successfully login
    Then manager sees Expenses Analysis Chart in pieChart view
    Then manager sees Expenses Analysis Chart in BarChart view
    Then manager sees Expenses Analysis Chart in LineChart view