package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.net.MalformedURLException;

public class LoginPage {

    public LoginPage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//a[contains(text(),'BriteErpDemo')]")
    public WebElement BriteErpDemo;

    @FindBy(css = "#login")
    public WebElement username;

    @FindBy(css = "#password")
    public WebElement password;

    public void login(String usernameText, String passwordText){
        username.sendKeys(usernameText);
        password.sendKeys(passwordText);
    }
}
