package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import static utilities.BrowserUtilities.waitForVisibility;


public class HomePage {
    public HomePage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath = "//a[@class='oe_menu_toggler']/span[contains(text(),'Expenses')]")
    public WebElement expensesLink;


    @FindBy(xpath = "//button[@accesskey='c']")
    public WebElement createButton;

    @FindBy(xpath = "//input[@placeholder='e.g. Lunch with Customer']")
    public WebElement expenseDescription;

    @FindBy(xpath = "//input[@class='o_input ui-autocomplete-input']")
    public WebElement product;

    @FindBy(xpath = "//li[@class='ui-menu-item']")
    public WebElement hiddenElement;

    @FindBy(xpath = "//input[@class='o_input']")
    public WebElement unitPrice;

    @FindBy(xpath = "//input[@class='o_field_float o_field_number o_field_widget o_input o_required_modifier oe_inline']")
    public WebElement quantity;

    @FindBy(xpath = "//input[@class='o_field_char o_field_widget o_input']")
    public WebElement billReference;

    @FindBy(xpath = "(//input[@class='o_input ui-autocomplete-input'])[4]")
    public WebElement employee;

    @FindBy(xpath = "(//li[@class='ui-menu-item'])[9]/a")
    public WebElement employeeHidden;

    @FindBy(css = "#o_field_input_254")
    public WebElement saleOrder;

    @FindBy(xpath = "//button[@accesskey='s']")
    public WebElement saveButton;

    @FindBy(xpath = "//div[@class='o_thread_message_content']/p")
    public WebElement successMessage;

    @FindBy(xpath = "//div[@class='o_notification_manager']")
    public WebElement warningMessage;

    @FindBy(xpath = "//span[contains(text(),'Expenses Analysis')]")
    public WebElement expensesAnalysis;
    @FindBy(xpath = "//button[@class='btn btn-default fa fa-pie-chart o_graph_button']")
    public WebElement pieChartButton;
    @FindBy (xpath = "//*[@class='nvd3 nv-wrap nv-pieChart']")
    public WebElement pieChartGraph;
    @FindBy(xpath = "//*[@class='btn btn-default fa fa-bar-chart-o o_graph_button']")
    public WebElement barChartButton;
    @FindBy(xpath = "//*[@class='nvd3 nv-wrap nv-multiBarWithLegend']")
    public WebElement barChartGraph;
    @FindBy(xpath = "//*[@class='btn btn-default fa fa-line-chart o_graph_button active']")
    public WebElement lineChartButton;
    @FindBy(xpath = "//*[@class='nv-focus']")
    public WebElement lineChartGraph;





}
