package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "@target/rerun.txt",
        plugin = "rerun:target/rerun.txt",
        glue = "step_definitions"
)
public class Failed_Runner {
    
}
