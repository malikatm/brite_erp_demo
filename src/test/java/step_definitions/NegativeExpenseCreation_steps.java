package step_definitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import org.junit.Assert;

public class NegativeExpenseCreation_steps {
HomePage hp = new HomePage();
    @When("user leave expense report blank")
    public void user_leave_expense_report_blank() {
        System.out.println("Leaving form blank");
    }

    @When("user clicks on save button")
    public void user_clicks_on_save_button() {
       hp.saveButton.click();
    }

    @Then("user should see a pop up notification")
    public void user_should_see_a_pop_up_notification() {
        String a = hp.warningMessage.getText();

        Assert.assertTrue( a.contains("The following fields are invalid:"));
    }

}
