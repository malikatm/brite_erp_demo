package step_definitions;


import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import pages.LoginPage;
import utilities.ConfigurationReader;
import utilities.Driver;
import utilities.Log;

import java.net.MalformedURLException;

public class Hooks {

    public Hooks() throws MalformedURLException {
    }

    @Before
    public void setUp(Scenario scenario){
        Log.startLog("Starting testing");
        Log.info("User goes to Brite ERP website");
        Driver.getDriver().get(ConfigurationReader.getProperty("url"));

    }

    @After
    public  void tearDown(Scenario scenario) throws MalformedURLException {

        if(scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.BYTES);

            scenario.embed(screenshot, "image/png");
        }
        Log.endLog("Test is ending");

        Driver.closeDriver();

    }
}
