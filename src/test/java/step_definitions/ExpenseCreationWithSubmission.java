package step_definitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import utilities.BrowserUtilities;
import utilities.Driver;

public class ExpenseCreationWithSubmission {


    @When("user should be able to enter {string} to expense description box.")
    public void user_should_be_able_to_enter_to_expense_description_box(String string) {

        Driver.getDriver().findElement(By.xpath("//input[@name='name']")).sendKeys(string);
    }

    @When("user selects {string} from drop down.")
    public void user_selects_from_drop_down(String string) {
        Driver.getDriver().findElement(By.id("o_field_input_19")).sendKeys(string);
        Driver.getDriver().findElement(By.id("o_field_input_19")).sendKeys(Keys.ENTER);
    }

    @When("user should be able to select required employee.")
    public void user_should_be_able_to_select_required_employee() throws InterruptedException {

        Driver.getDriver().findElement(By.id("o_field_input_27")).sendKeys("David Samson");

        Thread.sleep(3000);
        Driver.getDriver().findElement(By.id("o_field_input_27")).sendKeys(Keys.ENTER);

    }

    @When("user enters additional notes in notes box")
    public void user_enters_additional_notes_in_notes_box() {

        Driver.getDriver().findElement(By.xpath("//textarea[@id='o_field_input_36']"))
                .sendKeys("Check will be provided."+Keys.ENTER);
    }

    @Then("click on \\{string} button")
    public void click_on_button() {

        Driver.getDriver().findElement(By.xpath("//div[@class='o_statusbar_buttons']/button")).click();


    }

}
