package step_definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import pages.HomePage;
import pages.LoginPage;
import utilities.ConfigurationReader;
import utilities.Log;


import static utilities.BrowserUtilities.waitForClickablility;
import static utilities.BrowserUtilities.waitForVisibility;


public class ExpenseCreation_steps {
    HomePage homePage = new HomePage();
    LoginPage lp = new LoginPage();

    @Given("user successfully login")
    public void user_successfully_login() throws InterruptedException {
        String log4jConfPath = "log4j.properties";
        PropertyConfigurator.configure(log4jConfPath);

        lp.BriteErpDemo.click();
        lp.login(ConfigurationReader.getProperty("email"),ConfigurationReader.getProperty("password")+Keys.ENTER);
        waitForClickablility(homePage.expensesLink, 10);

    }


    @When("user clicks on create button")
    public void user_clicks_on_create_button() {
       waitForClickablility(homePage.expensesLink, 30);

       Log.info("user clicks on expenses link");
       homePage.expensesLink.click();
       waitForClickablility(homePage.createButton, 30);

       Log.info("clicks on create button");
       homePage.createButton.click();
       waitForVisibility(homePage.expenseDescription, 30);
    }

    @When("user fill out expenses form")
    public void user_fill_out_expenses_form() {
      Log.info("user fills out th form");
        createForm();
    }


    @Then("user should see confirmation that expense is created")
    public void user_should_see_confirmation_that_expense_is_created() {

        waitForVisibility(homePage.successMessage, 10);
        Log.info("verify success message");
        Assert.assertTrue(homePage.successMessage.getText().equals("Expense created"));
    }







    public void createForm(){
        homePage.expenseDescription.sendKeys(ConfigurationReader.getProperty("productDescription"));
        homePage.product.click();
        waitForVisibility(homePage.hiddenElement, 10);
        homePage.hiddenElement.click();

        waitForVisibility(homePage.unitPrice, 10);
        homePage.unitPrice.clear();
        homePage.unitPrice.sendKeys("20.00");
        homePage.quantity.clear();
        homePage.quantity.sendKeys("10.00");
        homePage.billReference.sendKeys("39999");
        homePage.employee.click();
        waitForVisibility(homePage.employeeHidden, 10);
        homePage.employeeHidden.click();

        homePage.saveButton.click();
    }


}
