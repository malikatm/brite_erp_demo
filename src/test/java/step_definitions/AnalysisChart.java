package step_definitions;

import cucumber.api.java.en.Then;
import pages.HomePage;
import org.junit.Assert;

import static utilities.BrowserUtilities.waitForClickablility;

public class AnalysisChart {
    HomePage hp = new HomePage();

    @Then("manager sees Expenses Analysis Chart in pieChart view")
    public void manager_sees_Expenses_Analysis_Chart_in_pieChart_view() {


        waitForClickablility(hp.expensesLink, 10);

        hp.expensesLink.click();
        hp.expensesAnalysis.click();
        hp.pieChartButton.click();
        Assert.assertTrue(hp.pieChartGraph.isDisplayed());

    }
    @Then("manager sees Expenses Analysis Chart in BarChart view")
    public void manager_sees_Expenses_Analysis_Chart_in_BarChart_view() {
        hp.barChartButton.click();
        Assert.assertTrue(hp.barChartGraph.isDisplayed());
    }

    @Then("manager sees Expenses Analysis Chart in LineChart view")
    public void manager_sees_Expenses_Analysis_Chart_in_LineChart_view() {
        hp.lineChartButton.click();
        Assert.assertTrue(hp.lineChartGraph.isDisplayed());
    }




}
