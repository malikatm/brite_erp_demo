@negative
@smoke
Feature: Testing expenses module website

  Scenario: Testing creating new expense report with blank form

    Given user successfully login
    When user clicks on create button
    And user leave expense report blank
    And user clicks on save button
    Then user should see a pop up notification