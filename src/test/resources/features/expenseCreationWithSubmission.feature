@submission
@smoke
Feature: Testing my expense creation with submission

  Scenario: Verify submit button functionality

    Given user successfully login
    When user clicks on create button
    And user should be able to enter "Out of state trip" to expense description box.
    When user selects "[CarTRA] Car Travel Expenses" from drop down.
    And user should be able to select required employee.
    And user enters additional notes in notes box
    Then click on {string} button